.. mlyPipeline documentation master file, created by
   sphinx-quickstart on Wed Apr 27 03:27:30 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MLy-Pipeline's documentation!
========================================

MLy-Pipeline is a collection of python scripts that use MLy package tools to create low-latency, gravitational wave search of sub-second signals with generic morphologies. This documentation was created for the need of reviewing MLy-Pipeline.

.. toctree::
   :caption: Contents:

.. note::

   This project and its documentation are under active development.
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Contents
--------

.. toctree::
    :maxdepth: 2
    
    installation
    settingupasearch
    runningasearch
    runningasearchoffline
    runningasearchonline