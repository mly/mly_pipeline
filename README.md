# MLy-Pipeline

All-Sky burst search framework to use machine learning models.

## Installation

Create an virtual python environment with python version 3.8 to 3.10. 
Then install the hermes submodules that are used by the pipeline and currently are not on pypl
```bash
pip install "git+https://github.com/ML4GW/hermes.git@12e19f1#subdirectory=hermes/hermes.aeriel"
pip install "git+https://github.com/ML4GW/hermes.git@12e19f1#subdirectory=hermes/hermes.quiver"
pip install "git+https://github.com/ML4GW/hermes.git@12e19f1#subdirectory=hermes/hermes.stillwater"
```

Finally install the module
```bash
pip install mly-pipeline
```

## Usage

For the full documentation and usesage examples please refer to here: https://ldas-jobs.ligo.caltech.edu/~vasileios.skliris/mly-documentation/html/index.html