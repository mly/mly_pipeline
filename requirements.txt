# development requirements for mly_pipeline
# - formal runtime requirements should be in pyproject.toml
# - only extra/pre-release packages should be included here

hermes.aeriel @ git+https://github.com/ML4GW/hermes.git@12e19f1#subdirectory=hermes/hermes.aeriel
hermes.quiver @ git+https://github.com/ML4GW/hermes.git@12e19f1#subdirectory=hermes/hermes.quiver
hermes.stillwater @ git+https://github.com/ML4GW/hermes.git@12e19f1#subdirectory=hermes/hermes.stillwater
